# Introduction to Python for Physicists 

In this repository, you can find the source code of the material developed for an introduction to Python for pysicists that runs as a "minicourse" of 1 ECTS as part of the "Inleidend Practicum" (introduction lab course) of the Applied Physics program at TU Delft.

The course consists of 5 core Jupyter notebooks designed for self-study. The notebooks are self-contained, and include an explanation of the concepts, example code to illustrate the concepts, and exercises (with answers at the end) for testing your knowledge. 

The notebooks are designed for people with no programming background at all, and are used in the course during 5 afternoons (20  hours). By the end  of the course, you should be familiar with:

* Basic concepts in Python: What it is and how it works
* Functions in Python: How to write them and how to use them 
* Program flow control: How to control the flow of execution of your code
* Scientific computing in Python: Introduction to  the numpy library
* Data in Python: How to load, plot and fit data

Each notebook includes a list of detailed learning objectives so you know what you should be learning. In addition, there is a "notebook for nerds" for exploring additional programming concepts in python. 

There are also assignment notebooks from the course, which can be shared upon [request](mailto:g.a.steele@tudelft.nl).

#  How to use them

If you want to use the notebooks for learning Python, you can download this zip file:

[Lecture_Notebooks.zip](https://gitlab.tudelft.nl/python-for-applied-physics/practicum-lecture-notes/-/jobs/artifacts/master/download?job=outputs)

The zipfile also includes instructions on how to  install Python and start up the notebook server. 

# Feedback

Did you find a typo? Is there something that is not clear to you? Is there a mistake in the notebooks? We gladly welcome feedback! To give feedback, the easiest for us is  for you to  submit an "issue" in our  repository issue tracker:

[Submit an "issue"](https://gitlab.tudelft.nl/python-for-applied-physics/practicum-lecture-notes/issues/new?issuable_template=feedback)

In the message, please include:

* The notebook number
* Cut-and-paste the text from the notebook
* Optional: Describe your suggestion (with a typo not needed even)

Feedback by [email](mailto:g.a.steele@tudelft.nl) is also welcome.


# Developers

The notebooks were developed by Gary Steele <g.a.steele@tudelft.nl> with input and feedback frpm Jeroen Kalkman <J.Kalkman@tudelft.nl> and Freek Pols <c.f.j.pols@tudelft.nl>.

```python

```
