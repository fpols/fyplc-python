ln -s ../resource . 
wget -O 'Additional Programming Concepts in Python.ipynb' https://gitlab.tudelft.nl/python-for-applied-physics/practicum-lecture-notes/-/raw/master/Additional%20Material/Additional%20Programming%20Concepts%20in%20Python.ipynb 
cd ../resource/asnlib/public
wget -O 'call_by_reference.png' https://gitlab.tudelft.nl/python-for-applied-physics/practicum-lecture-notes/-/raw/master/Additional%20Material/resource/asnlib/public/call_by_reference.png 
wget -O 'call_by_value.png' https://gitlab.tudelft.nl/python-for-applied-physics/practicum-lecture-notes/-/raw/master/Additional%20Material/resource/asnlib/public/call_by_value.png 
wget -O 'big_error_1.png' https://gitlab.tudelft.nl/python-for-applied-physics/practicum-lecture-notes/-/raw/master/Additional%20Material/resource/asnlib/public/big_error_1.png 
wget -O 'big_error_2.png' https://gitlab.tudelft.nl/python-for-applied-physics/practicum-lecture-notes/-/raw/master/Additional%20Material/resource/asnlib/public/big_error_2.png 
